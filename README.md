# My project's README

Installation:
1. Install node.js and mongodb
2. open terminal, go to folder CUJP
3. type 'npm install'

Run:
1. open terminal
2. type 'mongod' to start mongodb
3. open another terminal, go to folder CUJP
4. type 'node bin/www' to start the service
5. http://localhost:3000

Insert, query, update, delete, create scheme in mongoose
https://scotch.io/tutorials/using-mongoosejs-in-node-js-and-mongodb-applications

Mongoose Guide
http://mongoosejs.com/docs/guide.html

9 April, 2016 - Example at dev.ejs
Access: Login -> http://localhost:3000/dev.ejs
Now have:
add Job, apply Job, unapply Job, delete Job, edit Job

5 April, 2016 - Accidentally deleted all pervious commit records

