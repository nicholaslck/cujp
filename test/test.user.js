var should = require("should");
var mongoose = require('mongoose');
var Account = require("../models/account.js");
var Job = require("../models/job.js");
var db;

describe('Account', function() {

    before(function(done) {
        db = mongoose.connect('mongodb://localhost/test');
            done();
    });

    after(function(done) {
        mongoose.connection.close();
        done();
    });

    beforeEach(function(done) {
        var account = new Account({
            username: '12345@apple.com',
            password: 'testy'
        });

        account.save(function(error) {
            if (error) console.log('error' + error.message);
            else console.log('no error');
            done();
        });
    });

    it('find a user by username', function(done) {
        Account.findOne({ username: '12345@apple.com' }, function(err, account) {
            account.username.should.eql('12345@apple.com');
            console.log("   username: ", account.username);
            done();
        });
    });

    it('applyJob', function(done) {
        Account.findOne({ username: '12345@apple.com' }, function(err, account) {

          var job = new Job({
            title: 'test job',
            detail: 'nothing need to do',
            start: new Date(),
            end: new Date(),
            deadLine: new Date()
          });

          account.intersedJob.push(job._id);
          account.save(function(err) {
            if (err) throw err;
              job.candidates.push(account._id);
              job.save(function(err) {
                if (err) throw err;
                done();
              });
          });

        });
    });

    it('loadlIntersedJobs', function(done) {
        Account.findOne({ username: '12345@apple.com' }, function(err, account) {

          var job = new Job({
            title: 'test job',
            detail: 'nothing need to do',
            start: new Date(),
            end: new Date(),
            deadLine: new Date()
          });

          var job2 = new Job({
            title: 'test2 job',
            detail: 'nothing need to do',
            start: new Date(),
            end: new Date(),
            deadLine: new Date()
          });

          account.intersedJob.push(job._id);
          account.save(function(err) {
            if (err) throw err;
              job.candidates.push(account._id);
              job.save(function(err) {
                if (err) throw err;

                account.loadlIntersedJobs( function(error, jobs) {
                  if (error) console.log('error' + error.message);
                  console.log('JOBS------')
                  console.log(jobs);
                  done();
                });

              });
          });



        });
    });

    afterEach(function(done) {
        Account.remove({}, function() {
            done();
        });
     });

});
