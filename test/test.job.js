var should = require("should");
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Job = require("../models/job.js");
var db;

describe('Job', function() {

    before(function(done) {
        db = mongoose.connect('mongodb://localhost/test');
            done();
    });

    after(function(done) {
        mongoose.connection.close();
        done();
    });

var the_id;

    beforeEach(function(done) {
        var job = new Job({
          title: 'test job',
          detail: 'nothing need to do',
          start: new Date(),
          end: new Date(),
          deadLine: new Date()
        });

        job.save(function(error) {
            if (error) console.log('error' + error.message);
            else console.log('no error');
            the_id = job._id;
            done();
        });
    });

    it('find a job by title', function(done) {
        Job.findOne({ title: 'test job' }, function(error, job) {
            if (error) console.log('error' + error.message);
            else console.log('no error');
            job.title.should.eql('test job');
            console.log("   _id: ", job._id);
            console.log("   title: ", job.title);
            console.log("   detail: ", job.detail);
            console.log("   start: ", job.start);
            console.log("   end: ", job.end);
            console.log("   deadLine: ", job.deadLine);
            done();
        });
    });

    it('find a job by _id', function(done) {
        Job.findOne({ _id: ObjectId(the_id) }, function(error, job) {
            if (error) console.log('error' + error.message);
            else console.log('no error');
            job._id.should.eql(the_id);
            done();
        });
    });

    it('find jobs by month', function(done) {
        Job.jobByMonth(new Date(), function (error, jobs) {
          if (error) console.log('error' + error.message);
          else console.log('no error');
          console.log(jobs);
          done();
        });
    });

    afterEach(function(done) {
        Job.remove({}, function() {
            done();
        });
     });
});
