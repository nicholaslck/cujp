var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var Job = require('../models/job');
var Interview = require('../models/interview');
var Offer = require('../models/offer');
var sendgrid = require("sendgrid")("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");
var randomstring = require("randomstring");

var path = require("path");
var crypto = require("crypto");
var multer = require('multer');

var storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            if (err) return cb(err)

            cb(null, raw.toString('hex') + path.extname(file.originalname))
        })
    }
})

var router = express.Router();
//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

//file uploading
var upload = multer({ storage: storage });

var redirectHome = "<br> ......redirecting to home" +
                    "<meta http-equiv=\"refresh\" content=\"2; url=http://localhost:3000/home\" />"

router.get('/', function (req, res) {
    //res.render('index', { user : req.user });
    if(req.user) {
      res.redirect('home');
    } else {
      res.redirect('index');
    }
});

router.get('/videoDemo', function (req, res) {
      res.render('videoDemo');
});


router.get('/index', function (req, res) {
    //res.render('index', { user : req.user });
    if(req.user) {
      res.redirect('home');
    } else {
      res.render('index', {message: req.flash('error')});
    }
});

router.get('/video', function (req, res) {
      res.render('interviewRoom');
});

router.get('/interview', isLoggedIn, function (req, res) {
    Job.find( {}, function(err, jobs) {
        if(err) req.flash('devError', err.message);

        Interview.find( {ownerId: req.user._id}, function(err, hostedInterviews) {
            if(err) req.flash('devError', err.message);

            var hostedInterviewsJobId = hostedInterviews.map(function(interview) {
                return interview.jobId;
            });

            Job.find({ _id: { $in: hostedInterviewsJobId} }, function(err, interviewJobs) {
                if(err) req.flash('devError', err.message);

                Interview.find( {candidate: req.user._id}, function(err, candidateInterviews) {
                    if(err) req.flash('devError', err.message);

                    var candidateInterviewsJobId = candidateInterviews.map(function(interview) {
                        return interview.jobId;
                    });

                    Job.find({ _id: { $in: candidateInterviewsJobId} }, function(err, candidateInterviewJobs) {
                        if(err) req.flash('devError', err.message);
                        res.render('interview', {
                            info: req.flash('devError'),
                            jobs: JSON.parse(JSON.stringify(jobs)),
                            user: req.user,
                            hostedInterviews: hostedInterviews,
                            interviewJobs: interviewJobs,
                            candidateInterviews: candidateInterviews,
                            candidateInterviewJobs: candidateInterviewJobs
                        });
                    });

                });

            });

        });
    });
});

router.get('/dev', isLoggedIn, function(req, res) {

    // Job.find( {}, function(err, jobs) {
    //     if(err) req.flash('devError', err.message);
    //     res.render('dev', {
    //       info: req.flash('devError'),
    //       jobs: JSON.parse(JSON.stringify(jobs)),
    //       user: req.user
    //     });
    // });

    // Interview.find( {ownerId: req.user._id}, function(err, hostedInterviews) {
    //   if(err) req.flash('devError', err.message);
    //
    //   var hostedInterviewsJobId = hostedInterviews.map(function(interview) {
    //     return interview.jobId;
    //   });
    //
    //   Job.find({ _id: { $in: hostedInterviewsJobId} }, function(err, interviewJobs) {
    //       if(err) req.flash('devError', err.message);
    //       console.log(hostedInterviews);
    //       console.log(interviewJobs);
    //       res.status(200).send();
    //   });
    //
    // });

    // Interview.find( {candidate: req.user._id}, function(err, candidateInterviews) {
    //   if(err) req.flash('devError', err.message);
    //
    //   var candidateInterviewsJobId = candidateInterviews.map(function(interview) {
    //     return interview.jobId;
    //   });
    //
    //   Job.find({ _id: { $in: candidateInterviewsJobId} }, function(err, candidateInterviewJobs) {
    //       if(err) req.flash('devError', err.message);
    //       console.log(candidateInterviews);
    //       console.log(candidateInterviewJobs);
    //       res.status(200).send();
    //   });
    //
    // });

    // Job.find( {}, function(err, jobs) {
    //     if(err) req.flash('devError', err.message);
    //
    //     Interview.find( {ownerId: req.user._id}, function(err, hostedInterviews) {
    //       if(err) req.flash('devError', err.message);
    //
    //       var hostedInterviewsJobId = hostedInterviews.map(function(interview) {
    //         return interview.jobId;
    //       });
    //
    //       Job.find({ _id: { $in: hostedInterviewsJobId} }, function(err, interviewJobs) {
    //           if(err) req.flash('devError', err.message);
    //
    //           res.render('dev', {
    //             info: req.flash('devError'),
    //             jobs: JSON.parse(JSON.stringify(jobs)),
    //             user: req.user,
    //             hostedInterviews: hostedInterviews,
    //             interviewJobs: interviewJobs
    //           });
    //       });
    //
    //     });
    // });

    Job.find( {}, function(err, jobs) {
        if(err) req.flash('devError', err.message);

        Interview.find( {ownerId: req.user._id}, function(err, hostedInterviews) {
          if(err) req.flash('devError', err.message);

          var hostedInterviewsJobId = hostedInterviews.map(function(interview) {
            return interview.jobId;
          });

          Job.find({ _id: { $in: hostedInterviewsJobId} }, function(err, interviewJobs) {
              if(err) req.flash('devError', err.message);

              Interview.find( {candidate: req.user._id}, function(err, candidateInterviews) {
                if(err) req.flash('devError', err.message);

                var candidateInterviewsJobId = candidateInterviews.map(function(interview) {
                  return interview.jobId;
                });

                Job.find({ _id: { $in: candidateInterviewsJobId} }, function(err, candidateInterviewJobs) {
                    if(err) req.flash('devError', err.message);
                    res.render('dev', {
                      info: req.flash('devError'),
                      jobs: JSON.parse(JSON.stringify(jobs)),
                      user: req.user,
                      hostedInterviews: hostedInterviews,
                      interviewJobs: interviewJobs,
                      candidateInterviews: candidateInterviews,
                      candidateInterviewJobs: candidateInterviewJobs
                    });
                });

              });

          });

        });
    });

    // Interview.find( {candidate: req.user._id}, function(err, candidateInterview) {
    //   if(err) req.flash('devError', err.message);
    // });


});

router.post('/jobs/offer/:token', function (req, res) {
  var token = req.params.token;

  var object = req.body;

  console.log(object);
  console.log(token);

  Offer.findOneAndUpdate({token: token}, {status: true}, function (err, offer) {
      if (err) return res.status(400).send(err.message);

      Job.findById(offer.jobId, function (error, job) {
        if (err) return res.status(400).send(err.message);
        if (!job) return res.status(400).send('null job');

        var candidatesId = job.candidates.map(function(candidate) {return candidate.id;});
        var objectIndex = candidatesId.map(String).indexOf(String(offer.candidate));

        if (objectIndex <= -1) return res.status(400).send('NO SUCH OFFER');
        job.candidates[objectIndex].status = 'ACCEPTED';
        job.markModified('candidates');

        console.log("objectIndex " + objectIndex);
        console.log(job.candidates);

        job.save(function(err) {
          if (err) return res.status(400).send(err.message);
          res.status(200).send("Offer accpeted, please arrive on time" + redirectHome);
        });

      });

  });

});

router.get('/jobs/offer/:token', function (req, res) {
  var token = req.params.token;

  Offer.findOne({token: token}, function (err, offer) {
      if (err) return res.status(400).send(err.message);
      if (!offer) return res.status(400).send("Invalid token" + redirectHome);
      if (offer.status) return res.status(400).send("Already accpeted" + redirectHome);

      Job.findById(offer.jobId, function (err, job) {
        if (err) return res.status(400).send(err.message);
        if (!job) return res.status(400).send("Invalid token" + redirectHome);
        res.render('confirmOffer', {offer: offer, job: job} );
      });
  });

});

router.post('/jobs/offer', isLoggedIn, function (req, res) {
    var object = req.body;
    console.log(object);

    var candidate = JSON.parse(object.candidate);
    var job = JSON.parse(object.job);

    // console.log(candidate);
    // console.log(job);

    if (req.user._id != job.ownerId) return res.status(400).send('You are not authorized');

    var offer = new Offer({
      ownerId: req.user._id,
      jobId: job._id,
      candidate: candidate._id,
      token: randomstring.generate(),
      status: false
    });

    offer.save( function(err) {
      if (err) return res.status(400).send(err.message);
      var email = new sendgrid.Email();
      email.addTo(candidate.username);
      email.setFrom("non-reply@CUJP.com");
      email.setSubject("CUJP - " + job.title + " offer ");
      email.setHtml("Access the following link to accpet/decline " + job.title + " offer:"
                    + "<br>"
                    + "<a href=\"http://localhost:3000/jobs/offer/" + offer.token + "\">"
                    + "http://localhost:3000/jobs/offer/" + offer.token
                    + "</a>");
      sendgrid.send(email);

      res.redirect(req.get('referer'));

    });

});


router.get('/history', isLoggedIn, function (req, res) {

    Offer.find( {candidate: req.user._id, status: true}, function(err, offers) {
        if (err) return res.status(400).send(err.message);
        console.log(offers);
        var offerJobsId = offers.map( function(offer) { return offer.jobId; } );
        Job.find({_id: { $in : offerJobsId}}, function(err, jobs) {
          if (err) return res.status(400).send(err.message);
          res.render('history', {jobs: jobs, offers: offers, user: req.user});
        });
    });

});

router.post('/history', isLoggedIn, function (req, res) {

    var object = req.body.viewUser;
    var viewUser = JSON.parse(object);

    console.log(viewUser);

    Offer.find( {candidate: viewUser._id, status: true}, function(err, offers) {
        if (err) return res.status(400).send(err.message);
        console.log(offers);
        var offerJobsId = offers.map( function(offer) { return offer.jobId; } );
        Job.find({_id: { $in : offerJobsId}}, function(err, jobs) {
            if (err) return res.status(400).send(err.message);
            res.render('history', {jobs: jobs, offers: offers, user: req.user});
        });
    });
});

router.post('/jobs/decline', isLoggedIn, function (req, res) {
    var object = req.body;
    console.log(object);

    var candidate = JSON.parse(object.candidate);
    var job = JSON.parse(object.job);

    if (req.user._id != job.ownerId) return res.status(400).send('You are not authorized');

    Job.findById(job._id, function(err, job) {
      if (err) return res.status(400).send(err.message);
      if (!job) return res.status(400).send('null job');
      var candidatesId = job.candidates.map( function(candidate) {return candidate.id;} );
      var objectIndex = candidatesId.map(String).indexOf(String(candidate._id));
      if (objectIndex > -1) {
        job.candidates[objectIndex].status = "DECLINE";
        job.markModified('candidates');
        job.save(function(err) {
          if (err) return res.status(400).send(err.message);
          res.redirect(req.get('referer'));
        });
      }
    });

});

router.post('/interview/room', isLoggedIn, function (req, res) {
    var object = req.body;
    console.log(object);

    var interview = JSON.parse(object.interview);
    var job = JSON.parse(object.job);

    // console.log(interview.ownerId);
    // console.log(req.user._id);
    // console.log(interview.candidate);

    if (interview.ownerId == req.user._id) {
      res.render('interviewRoom', {interview: interview, job: job, host: true, user: req.user});
    } else if (interview.candidate == req.user._id) {
      res.render('interviewRoom', {interview: interview, job: job, host: false, user: req.user});
    } else {
      res.status(400).send("You don't belongs here");
    }

});

router.get('/postNewJob', isLoggedIn, function (req, res) {
    res.render('postNewJob', { user : req.user } );
});

router.get('/register', function(req, res) {

    if(req.user) {
      res.redirect('home');
    } else {
      res.render('register', {info: req.flash('signupMessage') });
    }

});

// router.post('/jobs/apply', isLoggedIn, function(req, res) {
//
//    var json = new Job( JSON.parse(req.body.job) );
//    console.log(json);
//
//    Job.findById(json._id, function (err, job) {
//      if (err) {
//        req.flash('devError', err.message);
//        res.redirect('/dev');
//      }
//
//      if (job.candidates.map(String).indexOf(String(req.user._id)) > -1) {
//         req.flash('devError', 'You have already applied the job');
//         return res.redirect('/dev');
//      }
//
//      job.candidates.push(req.user._id);
//      job.save(function(err) {
//        if (err) req.flash('devError', err.message);
//        res.redirect('/dev');
//      });
//   });
//
// });

router.post('/jobs/interview', isLoggedIn, function(req, res) {

  var object = req.body;
  console.log(object);

  var interview = new Interview({
    ownerId: req.user._id,
    jobId: object.jobId,
    from:  new Date(object.from),
    to:    new Date(object.to),
    candidate: object.candidate,
    triggerToken: randomstring.generate(),
    confirmed: false
  });

  interview.save(function(err) {
    if (err) req.flash('devError', err);
    console.log('Interview: ' + interview._id + ' has been arranged!');

    var email = new sendgrid.Email();
    email.addTo(object.username);
    email.setFrom("non-reply@CUJP.com");
    email.setSubject("Interview invitation");
    email.setHtml("Access the following link to confirm " + object.jobTitle + " interview:"
                  + "<br>"
                  + "<a href=\"http://localhost:3000/jobs/interview/" + interview.triggerToken + "\">"
                  + "http://localhost:3000/jobs/interview/" + interview.triggerToken
                  + "</a>");
    sendgrid.send(email);

    res.redirect(req.get('referer'));
  });

});

router.get('/jobs/interview/:token', function(req, res) {

  var token = req.params.token;
  //console.log(token);

  Interview.findOne({triggerToken: token}, function (err, interview) {
      if (err) return res.status(400).send(err.message);
      if (!interview) return res.status(400).send("Invalid token" + redirectHome);
      if (interview.confirmed) return res.status(400).send("Already confirmed" + redirectHome);

      Job.findById(interview.jobId, function (err, job) {
        if (err) return res.status(400).send(err.message);
        if (!job) return res.status(400).send("Invalid token" + redirectHome);
        res.render('confirmInterview', {interview: interview, job: job} );
      });

  });

});

router.post('/jobs/interview/:token', function(req, res) {

  Interview.findOneAndUpdate({triggerToken: req.params.token}, {confirmed: true}, function (err, interview) {
      if (err) return res.status(400).send(err.message);
      res.status(200).send("Interview confirmed, please arrive on time" + redirectHome);
  });

});


router.get('/jobs/candidates', isLoggedIn, function(req, res) {



  var object = req.query;

  if (object.host != req.user._id) return res.status(400).send("YOU ARE NOT AUTHORIZED");

  Job.findById(object.id, function (err, job) {
    if (err) return res.status(400).send(err.message);
    if (!job) return res.status(400).send();

    job.loadlCandidates( function (error, accounts) {
      if (error) return res.status(400).send(error.message);
      //console.log(accounts);
      Interview.find( {ownerId: req.user._id, jobId: job._id}, function(err, hostedInterviews) {
        if (error) return res.status(400).send(error.message);
        var invitedCandidate = hostedInterviews.map(function(interview) {
          return interview.candidate;
        });

        Offer.find( {jobId: job._id}, function(err, offers) {
          if (err) return res.status(400).send(err.message);
          res.render('jobReview', { user: req.user, job: job, candidates: accounts, invitedCandidate: invitedCandidate, offers: offers });
        });

      });

    });

 });

});

router.post('/jobs/apply', isLoggedIn, function(req, res) {

   var json = new Job( JSON.parse(req.body.job) );
   console.log(json);

   Job.findById(json._id, function (err, job) {
     if (err) return res.status(400).send(err.message);
     if (!job) return res.status(400).send();

     var candidatesId = job.candidates.map(function(candidate) {return candidate.id;});

     if (candidatesId.map(String).indexOf(String(req.user._id)) > -1) {
        return res.status(400).send('You have already applied the job');
     }

     job.candidates.push({id: req.user._id, status: "PENDING"});
     job.save(function(err) {
       if (err) return res.status(400).send(err.message);
       return res.status(200).send();
     });
  });

});


router.delete('/jobs/apply', isLoggedIn, function(req, res) {

   var object = req.body;
   //console.log(object);
   console.log("DELETE JOB APPLY");

   Job.findById(object.id, function (err, job) {
     if (err) return res.status(400).send(err.message);
     if (!job) return res.status(400).send();

     job.candidates.pull({id: req.user._id, status: object.status});
     job.save(function(err) {
       if (err) return res.status(400).send(err.message);
       res.status(200).send();
     });

  });

});

router.post('/jobs/bookmark', isLoggedIn, function (req, res) {
    var object = req.body;


    Account.findById(req.user._id, function (err, user) {
        user.intersedJob.push(object.id);
        user.save(function(err) {
            if (err) return re.status(400).send(err.message);
            res.status(200).send();
        });
    });


});

router.delete('/jobs/bookmark', isLoggedIn, function(req, res) {
    var object = req.body;

    Account.findById(req.user._id, function (err, user) {
        user.intersedJob.pull(object.id);
        user.save(function(err) {
            if (err) return re.status(400).send(err.message);
            res.status(200).send();
        });
    });
});

router.delete('/jobs', isLoggedIn, function(req, res) {

   var object = req.body;

  //  console.log(object.host);
  //  console.log(req.user._id);

   if (object.host != req.user._id)
      return res.status(400).send('You are authorized to delete the job');

   Job.findByIdAndRemove(object.id, function (err) {
     if (err) return res.status(400).send(err.message);
     res.status(200).send();
   });

});

// router.get('/jobs/:id', isLoggedIn, function(req, res) {
//    var id = req.params.id;
//    /*
//    If user is job hoster, allow to access the candidates info
//    */
// });
//
// router.get('/jobs/apply/:id', isLoggedIn, function(req, res) {
//    var id = req.params.id;
//    /*
//    If user is job hoster, allow to access the candidates info
//    */
// });
//
// router.get('/jobs/search' , function(req, res) {
//   var keyword = req.query.keyword;
//    res.status(200).send("pong!" + keyword);
//    /*
//    If user is job hoster, allow to access the candidates info
//    */
// });
//
// router.get('/jobs/interview/:id', isLoggedIn, function(req, res) {
//    var id = req.params.id;
//    /*
//    If user is job hoster, allow to arrange the interview
//    */
// });
//
// router.get('/jobs/offer/:id', isLoggedIn, function(req, res) {
//    var id = req.params.id;
//    /*
//    If user is job hoster, allow to arrange the interview
//    */
// });

// router.get('/portfolio/:id', isLoggedIn, function(req, res) {
//    var id = req.params.id;
//
// });


router.get('/home', isLoggedIn, function(req, res) {

  Job.find( {}, function(err, jobs) {
      if(err) req.flash('devError', err.message);
      console.log(JSON.stringify(jobs));
      res.render('home', {
          info: req.flash('devError'),
          jobEvents: JSON.parse(JSON.stringify(jobs)),
          user: req.user
      });
  });

    // res.render('home', { user : req.user, jobEvents: null } );
});

router.get('/calender', isLoggedIn, function(req, res) {

    res.render('calender', { user : req.user } );
});

router.get('/jobs', isLoggedIn, function(req, res) {

    var keyword = req.query.keyword;

    if (keyword != null) {
        Job.find( {title: new RegExp(keyword, "i")}, function(err, jobs) {
            if(err) req.flash('devError', err.message);
            res.render('jobs', {
                info: req.flash('devError'),
                jobs: JSON.parse(JSON.stringify(jobs)),
                user: req.user
            });
        });
    } else {
        // Job.find( {}, function(err, jobs) {
        //     if(err) req.flash('devError', err.message);
        //     res.render('jobs', {
        //         info: req.flash('devError'),
        //         jobs: JSON.parse(JSON.stringify(jobs)),
        //         user: req.user
        //     });
        // });

        Job.find({}).sort({score: -1}).exec(function(err, jobs) {
          if(err) req.flash('devError', err.message);
          res.render('jobs', {
              info: req.flash('devError'),
              jobs: JSON.parse(JSON.stringify(jobs)),
              user: req.user
          });
        });
    }

    // res.render('jobs', { user : req.user, jobs : jobs } );
});

router.post('/portfolio/edit',upload.single('proPic'), isLoggedIn, function(req, res) {

    var object = req.body;
    object.proPic = req.file.filename;


    console.log(object);
    //res.status(200).send(object);

    Account.findByIdAndUpdate(req.user._id, object, function(err, doc) {
        if (err) return res.status(400).send(err.message);
        res.redirect(req.get('referer'));
    });


});

router.post('/portfolio', isLoggedIn, function(req, res) {

    var object = req.body.candidate;


    res.render('portfolio', { user : req.user, viewUser : JSON.parse(object) } );


});

router.get('/portfolio', isLoggedIn, function(req, res) {

    res.render('portfolio', { user : req.user , viewUser : req.user} );
});

router.get('/resume', isLoggedIn, function(req, res) {

    res.render('resume', {user : req.user, viewUser : req.user});
});

router.post('/resume', isLoggedIn, function (req, res) {
    var object = req.body.viewUser;
    console.log(object);

    res.render('resume', {user : req.user, viewUser : JSON.parse(object)});
});


router.post('/showBookmark', isLoggedIn, function (req, res) {
    var object = req.user;

    console.log(object);

    Job.find({_id: { $in : object.intersedJob}}, function(err, jobs) {
        res.render('bookmark', {
            jobs: jobs,
            user: req.user
        });
    });

});

router.post('/jobs', upload.single('image'), function(req, res) {
    if (req.file) {
        console.dir(req.file);
    }
    var object = req.body;
     console.log(req.file.imageName);

    var job = new Job({
      ownerId: req.user._id,
      title:  object.title,
      detail: object.detail,
      start:  new Date(object.start),
      end:    new Date(object.end),
      deadLine: new Date(object.deadLine),
      salaryPerHour: object.salaryPerHour,
      imageName: req.file.filename
    });

    job.save(function(err) {
      if (err) req.flash('devError', err);
      console.log('Job: ' + job._id + ' saved successfully!');
        console.log(job.imageName);

        res.redirect('jobs');
    });

});

router.post('/uploadResume', upload.single('pdf'), function(req, res) {
    if (req.file) {
        console.dir(req.file);
        var object = req.body;
        console.log(req.file.imageName);
        console.log(req.user);

        Account.findByIdAndUpdate(req.user._id, {resume: req.file.filename}, function(err, doc) {
            if (err) return res.status(400).send(err.message);
            return res.redirect('/resume');
        });



    }
});

router.post('/register', function(req, res, next) {

    if(req.user) {
      return res.redirect('home');
    }

    Account.register(new Account({
        username : req.body.username,
        fullName : req.body.fullName,
        gender   : req.body.gender,
        DOB      : req.body.DOB,
        phone    : req.body.phone,
        major    : req.body.major}), req.body.password, function(err, account) {
        if (err) {
          return res.render('register', {info: "Sorry. That username already exists. Try again."});
        }

        passport.authenticate('local')(req, res, function () {
            req.session.save(function (err) {
                if (err) {
                    return next(err);
                }
                res.redirect('/');
            });
        });
    });
});

// router.get('/login', function(req, res) {
//     res.render('login', { user : req.user, message : req.flash('error')});
// });
//
router.post('/login', passport.authenticate('local', { failureRedirect: '/index', failureFlash: true }), function(req, res, next) {
    req.session.save(function (err) {
        if (err) {
            return next(err);
        }
        res.redirect('/');
    });
});
//
router.get('/logout', function(req, res, next) {
    req.logout();
    req.session.save(function (err) {
        if (err) {
            return next(err);
        }
        res.redirect('/');
    });
});

router.post('/jobs/:id', isLoggedIn, function(req, res) {
    var object = req.body;
    // console.log(object);

    Job.findByIdAndUpdate(req.params.id, object, function(err, doc) {
        if (err) return res.status(400).send(err.message);
        return res.redirect('/dev');
    });

});


//
// router.get('/ping', function(req, res){
//     res.status(200).send("pong!");
// });

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

module.exports = router;
