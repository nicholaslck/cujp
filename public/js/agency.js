/*!
 * Start Bootstrap - Agency Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})


// Highlight the nav for different page
$(function () {

    //highlight nav
    var href = $(location).attr('href');
    console.log(href);
    if (href.indexOf("home") >= 0) {
    	$(".navbar a:contains('Home')").parent().addClass('active');
    } else if (href.indexOf("jobs") >= 0) {
        console.log("jobs parent");
        $(".navbar a:contains('Jobs')").parent().addClass('active');
    } else if (href.indexOf("interview") >= 0) {
        console.log("interview parent");
        $(".navbar a:contains('Interview')").parent().addClass('active');
    } else if (href.indexOf("calender") >= 0) {
    	$(".navbar a:contains('Calender')").parent().addClass('active');
    } else if (href.indexOf("history") >= 0) {
    	$(".navbar a:contains('History')").parent().addClass('active');
    } else if (href.indexOf("portfolio") >= 0) {
    	$(".navbar a:contains('Portfolio')").parent().addClass('active');
    }


});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});