var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Offer = new Schema({

    ownerId: Schema.Types.ObjectId,
    created_at: Date,
    updated_at: Date,

    jobId: Schema.Types.ObjectId,
    candidate: Schema.Types.ObjectId,
    token: String,

    comment: String,

    status: Boolean

});

module.exports = mongoose.model('offers', Offer);
