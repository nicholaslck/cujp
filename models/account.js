var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({

    username: String,
    password: String,

    fullName: String,
    DOB: Date,
    gender: String,
    phone: Number,

    major: String,
    resume: String,

    intersedJob: [Schema.Types.ObjectId],

    description: String,
    proPic: String

});

Account.plugin(passportLocalMongoose);

Account.statics.loadUesrById = function (id, cb) {
  return this.findById(id, cb);
}

Account.methods.loadlIntersedJobs = function (cb) {
  return this.model('jobs').find({ _id: { $in: this.intersedJob} }, cb);
}

Account.methods.loadResume = function (cb) {
  return this.model('resumes').findById(this.resume, cb);
}

module.exports = mongoose.model('accounts', Account);
