var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Resume = new Schema({

    ownerId: Schema.Types.ObjectId,
    created_at: Date,
    updated_at: Date,

    pdf: Buffer

});

Resume.pre('save', function(next) {
    var currentDate = new Date();
    this.updated_at = currentDate;
    if (!this.created_at)
      this.created_at = currentDate;
    next();
});

module.exports = mongoose.model('resumes', Resume);
