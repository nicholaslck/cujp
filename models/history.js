var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var JobHistory = new Schema({

    ownerId: Schema.Types.ObjectId,
    created_at: Date,
    updated_at: Date,

    jobId: Schema.Types.ObjectId,
    employeeId: Schema.Types.ObjectId,
    employerId: Schema.Types.ObjectId,

    employeeReview: String,
    employeeRating: Number,

    employerReview: String,
    employerRating: Number

});

JobHistory.methods.loadJob = function (cb) {
  return this.model('jobs').findById(this.jobId, cb);
}

JobHistory.pre('save', function(next) {
    var currentDate = new Date();
    this.updated_at = currentDate;
    if (!this.created_at)
      this.created_at = currentDate;
    next();
});

module.exports = mongoose.model('historys', JobHistory);
