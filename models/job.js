var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Job = new Schema({

    ownerId: Schema.Types.ObjectId,
    created_at: Date,
    updated_at: Date,

    title: String,
    detail: String,
    start: Date,
    end: Date,
    deadLine: Date,
    salaryPerHour: Number,
    imageName: String,

    candidates: [Schema.Types.Mixed],
    score: Number

});


Job.statics.jobByMonth = function (date, cb) {
  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
  var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  return this.find({ deadLine: {'$gte': firstDay, '$lt': lastDay} }, cb);
}

Job.methods.loadlCandidates = function (cb) {
  var candidatesId = this.candidates.map(function(candidate) {return candidate.id;});
  return this.model('accounts').find({ _id: { $in: candidatesId} }, cb);
}

// def hot(ups, downs, date):
//     s = score(ups, downs)
//     order = log(max(abs(s), 1), 10)
//     sign = 1 if s > 0 else -1 if s < 0 else 0
//     seconds = epoch_seconds(date) - 1134028003
//     return round(sign * order + seconds / 45000, 7)

Job.pre('save', function(next) {

    var currentDate = new Date();
    this.updated_at = currentDate;
    if (!this.created_at)
      this.created_at = currentDate;

    var s = this.candidates.length;
    var order = Math.log(Math.max(Math.abs(s), 1)) / Math.LN10;
    var sign = (s > 0) ? 1 : (s < 0) ? -1 : 0;
    var seconds = (this.created_at.getTime() / 1000) - 1134028003;
    this.score = (sign * order + seconds / 45000).toFixed(7);

    next();
});

module.exports = mongoose.model('jobs', Job);
