var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Interview = new Schema({

    ownerId: Schema.Types.ObjectId,
    created_at: Date,
    updated_at: Date,

    from: Date,
    to: Date,

    jobId: Schema.Types.ObjectId,
    candidate: Schema.Types.ObjectId,
    triggerToken: String,

    confirmed: Boolean

});

module.exports = mongoose.model('interviews', Interview);
